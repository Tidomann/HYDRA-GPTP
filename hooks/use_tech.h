#pragma once
#include <SCBW/structures.h>

namespace hooks 
{
	void BTNSACT_UseTech(u16 tech, GrpHead* head);
	void injectUseTechHooks();
} //hooks
